import './App.css';
import '@fontsource/roboto/300.css';
import '@fontsource/roboto/400.css';
import '@fontsource/roboto/500.css';
import '@fontsource/roboto/700.css';
import Login from './components/Login';
import AccountsList from './components/AccountsList';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  useParams,
} from 'react-router-dom';
import Accountdetail from './components/Accountdetail';

const initialState = {
  customer: {},
};

function reducer(state, action) {
  switch (action.type) {
    case 'SET_CUSTOMER':
      return { ...state, customer: action.payload };
    default:
      return state;
  }
}

const store = createStore(reducer, initialState);

function App() {
  return (
    <Provider store={store}>
      <Router>
        <Switch>
          <Route exact path='/' component={LoginPage} />
          <Route exact path='/accounts' component={AccountsPage} />
          <Route path='/accounts/:accountId' component={AccountDetailPage} />
        </Switch>
      </Router>
    </Provider>
  );
}

function LoginPage() {
  return <Login titleApp='App React - Demo' />;
}

function AccountsPage() {
  return <AccountsList />;
}

function AccountDetailPage() {
  const { accountId } = useParams();
  return <Accountdetail idAccount={accountId} />;
}

export default App;
