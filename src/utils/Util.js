export const URL_HOST = 'https://cal-glomo.bbva.pe/SRVS_A02';

export const buildBodyGT = (tdoi, numdoi, password) => {
  return {
    authentication: {
      consumerID: '13000013',
      userID: `${tdoi}${numdoi}`,
      authenticationType: '02',
      authenticationData: [
        {
          authenticationData: [password],
          idAuthenticationData: 'password',
        },
      ],
    },
    backendUserRequest: {
      userId: '',
      accessCode: `${tdoi}${numdoi}`,
      dialogId: '',
    },
  };
};

export const formatAccount = (number) => {
  try {
    const countryCode = number.substring(0, 4);
    const branchCode = number.substring(4, 8);
    const code = number.substring(8);
    return `${countryCode}-${branchCode}-${code}`;
  } catch (e) {
    return '';
  }
};

export const formatAmout = (specificAmounts, type) => {
  specificAmounts = specificAmounts || [];
  const data = specificAmounts.filter((item) => item.id === type)[0];
  const currency = data?.amounts[0]?.currency || 'USD';
  const amount = data?.amounts[0]?.amount || 0;
  let lang = 'en-US';
  if (currency === 'PEN') {
    lang = 'es-PE';
  }
  const formatter = new Intl.NumberFormat(lang, {
    style: 'currency',
    currency: currency,
  });

  return formatter.format(amount);
};

export const formatMoney = (amount, currency) => {
  amount = amount || 0;
  currency = currency || 'USD';
  let lang = 'en-US';
  if (currency === 'PEN') {
    lang = 'es-PE';
  }
  const formatter = new Intl.NumberFormat(lang, {
    style: 'currency',
    currency: currency,
  });

  return formatter.format(amount);
};

export const formatTypeNumberAccount = (formats, type) => {
  formats = formats || [];
  const data = formats.filter((item) => item.numberType.id === type)[0];
  const number = data?.number || '--';
  return number;
};

export const formatdate = (strDate) => {
  try {
    return strDate.substring(0,10).split('-').reverse().join('/');
  } catch(e) {
    return '';
  }
}
