import axios from 'axios';
import { URL_HOST, buildBodyGT } from '../utils/Util';

const buildHeaderConfig = () => {
  return {
    headers: {
      tsec: window.sessionStorage.getItem('tsec'),
    },
  };
};

export const loginAuth = async (tdoi, numdoi, password) => {
  try {
    const bodyGT = buildBodyGT(tdoi, numdoi, password);
    const response = await axios.post(
      `${URL_HOST}/TechArchitecture/pe/grantingTicket/V02`,
      bodyGT
    );
    window.sessionStorage.setItem('tsec', response.headers['tsec']);
    if (response.status === 200) {
      const config = {
        headers: {
          tsec: response.headers['tsec'],
        },
      };
      const customer = await axios.get(
        `${URL_HOST}/customers/v0/customers`,
        config
      );
      const financialOverview = await axios.get(
        `${URL_HOST}/financial-overview/v0/financial-overview`,
        config
      );
      window.sessionStorage.setItem(
        'customer',
        JSON.stringify(customer.data.data[0])
      );
      window.sessionStorage.setItem(
        'financialOverview',
        JSON.stringify(financialOverview.data.data)
      );
      return true;
    }
    return false;
  } catch (e) {
    return false;
  }
};

export const getContract = async (id) => {
  try {
    const response = await axios.get(`${URL_HOST}/accounts/v0/accounts/${id}`, buildHeaderConfig());
    console.log(response.data);
    return response.data;
  } catch (e) {
    console.log('getContract Error ',e);
    if(e.response.status === 403) {
      window.location.href = '/';
    }
  }
};

export const listTransactions = async (id) => {
  try {
    const response = await axios.get(`${URL_HOST}/accounts/v0/accounts/${id}/transactions`, buildHeaderConfig());
    console.log(response.data);
    return response.data;
  } catch (e) {
    console.log('listTransactions Error ',e);
    if(e.response.status === 403) {
      window.location.href = '/';
    }
  }
};
