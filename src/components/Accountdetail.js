import React from 'react';
import { useEffect, useState } from 'react';
import styled from 'styled-components';
import { getContract, listTransactions } from '../utils/Service';
import Header from './Header';
import Container from '@mui/material/Container';
import Box from '@mui/material/Box';
import { formatAccount, formatMoney, formatTypeNumberAccount, formatdate } from '../utils/Util';
import MaskLoading from './MaskLoading';

const AccountdetailStyled = styled.div`
 margin-top: 50px;
  table {
    border: 1px solid #ccc;
    border-collapse: collapse;
    margin: 0;
    padding: 0;
    width: 100%;
    table-layout: fixed;
  }

  table caption {
    font-size: 1.2em;
    margin: 0 0 0.2em 0;
    color: #1264a5;
    text-align: left ;
  }

  table tr {
    background-color: #fff;
    border: 1px solid #ddd;
    padding: 0.35em;
    cursor: pointer;
  }

  table tr:hover {
    background-color: #b3e5fc;
  }

  table th,
  table td {
    padding: 0.625em;
    text-align: left;
    padding: 12px 16px;
  }

  table td {
    font-weight: lighter;
  }

  table th {
    font-size: 0.85em;
    letter-spacing: 0.1em;
    text-transform: uppercase;
    background-color: #004481;
    color: #fff;
  }

  .detail-account {
    line-height: 1.3em;
    b {
      color: #1264a5;
    }
  }

  @media screen and (max-width: 600px) {
    table {
      border: 0;
    }

    table caption {
      text-align: left ;
      font-size: 1.05em;
    }

    table thead {
      border: none;
      clip: rect(0 0 0 0);
      height: 1px;
      margin: -1px;
      overflow: hidden;
      padding: 0;
      position: absolute;
      width: 1px;
    }

    table tr {
      border-bottom: 3px solid #ddd;
      display: block;
      margin-bottom: 0.625em;
      cursor: pointer;
    }

    table tr:hover {
      background-color: #b3e5fc;
    }

    table td {
      border-bottom: 1px solid #ddd;
      display: block;
      font-size: 0.8em;
      text-align: right;
    }

    table td::before {
      content: attr(data-label);
      float: left;
      font-weight: bold;
      text-transform: uppercase;
    }

    table td:last-child {
      border-bottom: 0;
    }
  }

  .info-account-container {
    display: flex;
    align-items: center;
    justify-content: space-between;
    margin: 0;
    padding-top: 15px;
    padding-bottom: 5px;
    padding-right: 10px;
    line-height: 1.4em;
    .box-left {
        display: flex;
        justify-content: flex-start;
        flex-direction: column;
        .number {
            font-size: 0.88em;
        }
        .text {
            font-weight: lighter;
        }
        .name {
            font-weight: bold;
            color: #666;
        }
    }
    .box-right {
        display: flex;
        justify-content: flex-flex-end;
        flex-direction: column;
        text-align: right;
        line-height: 1.3em;
        .text {
            font-weight: lighter;
        }
        .amount {
            font-weight: bold;
            color: #1565c0;
            margin-top: 3px;
            margin-bottom: 5px;
            font-size: 1.55em;
        }
    }
  }

`;

function Accountdetail({ idAccount }) {
  console.log('idAccount', idAccount);
  const [loading, setLoading] = React.useState(false);
  let customer = window.sessionStorage.getItem('customer');
  if (!customer) {
    window.location.href = '/';
  } else {
    customer = JSON.parse(customer);
  }
  const [detailContract, setDetailContract] = useState({});
  const [listContract, setListContract] = useState([]);
  const getData = async(idAccount) => {
    if (!idAccount) { return; }
    setLoading(true);
    const responseDetail = await getContract(idAccount);
    setDetailContract(responseDetail.data);
    const responseListTransactions = await listTransactions(idAccount);
    setListContract(responseListTransactions.data);
    setLoading(false);
  }
  useEffect(() => {
    getData(idAccount);
  }, []);

  return (
    <AccountdetailStyled>
      <MaskLoading show={loading} ></MaskLoading>
      <Header showActionBack={true} titleHeader={'BIENVENIDO, ' + customer.firstName}></Header>
      <Container maxWidth='lg'>
        <Box
          sx={{
            bgcolor: '#f4f4f4',
            borderLeft: '1px solid #e1e1e1',
            borderRight: '1px solid #e1e1e1',
            minHeight: '100vh',
            padding: '0 15px',
            marginBottom: '25px',
          }}
        >
          <div className="info-account-container" >
            <div className="box-left" >
                <span className="text number">{formatAccount(detailContract?.number)}</span>
                <small className="name">{detailContract?.title?.name}</small>
                <small className="text">Cuenta remesa: { formatTypeNumberAccount(detailContract?.formats, 'CCI') }</small>
                <small className="text">Cuenta interbancaria: { formatTypeNumberAccount(detailContract?.formats, 'LIC') }</small>
            </div>
            <div className="box-right" >
                <small className="text disponible">DISPONIBLE</small>
                <span className="amount">{ formatMoney(detailContract?.availableBalance?.currentBalances[0]?.amount, detailContract?.availableBalance?.currentBalances[0]?.currency) }</span>
                <small className="text">Contable: { formatMoney(detailContract?.availableBalance?.postedBalances[0]?.amount,detailContract?.availableBalance?.postedBalances[0]?.currency) }</small>

            </div>
          </div>  
          <table>
            <caption>Últimos movimientos</caption>
            <thead>
              <tr>
                <th scope='col'>Fecha</th>
                <th scope='col'>Descripción</th>
                <th scope='col'>Monto</th>
              </tr>
            </thead>
            <tbody>
              {(listContract || []).map((item) => {
                return (
                  <tr>
                    <td data-label='Fecha'>
                      { formatdate(item.operationDate) }
                    </td>
                    <td data-label='Descripción'>
                      { item.concept }
                    </td>
                    <td data-label='Monto'>
                    { formatMoney(item?.localAmount?.amount, item?.localAmount?.currency) }
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </Box>
      </Container>
    </AccountdetailStyled>
  );
}

export default Accountdetail;
