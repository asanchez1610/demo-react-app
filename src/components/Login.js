import React from 'react';
import styled from 'styled-components';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import { loginAuth } from '../utils/Service';
import MaskLoading from './MaskLoading';

const LoginStyled = styled.div`
  .login-container {
    background-color: #072146;
    width: 100vw;
    height: 100vh;
    display: flex;
    align-items: center;
    justify-content: center;
    overflow: hidden;
  }
  .login-container__form {
    width: 350px;
    background-color: rgba(0, 0, 0, 0.2);
    padding: 15px 20px;
    color: white;
    box-shadow: 10px -10px #006c6c;
  }
  .login-container__form h2 {
    margin: 0;
    font-weight: lighter;
    font-size: 1.2em;
    text-align: center;
    padding-bottom: 10px;
    color: white;
    border-bottom: 1px solid rgba(255, 255, 255, 0.3);
  }
  .login-container__form .form-body {
    padding: 15px 0 10px 0;
  }

  @media screen and (max-width: 320px) {
    .login-container__form {
      width: 88%;
      box-shadow: 5px -5px #006c6c;
      padding-bottom: 5px;
    }
  }

  .form-white {
    background-color: white;
    margin-bottom: 3px;
  }
  .form-button {
    padding: 0;
    margin: 0;
    margin-top: 5px;
  }
  .form-button Button {
    background-color: #006c6c;
  }
`;

function Login({ titleApp }) {
  const [tDoi, setTDoi] = React.useState('L');
  const [numDocument, setNumDocument] = React.useState('');
  const [password, setPassword] = React.useState('');
  const [validNumDocumento, setValidNumDocumento] = React.useState(true);
  const [validPassword, setValidPassword] = React.useState(true);
  const [loading, setLoading] = React.useState(false);
  const changeTDoi = (event) => {
    setTDoi(event.target.value);
  };
  const changeNumDocument = (event) => {
    setNumDocument(event.target.value);
  };
  const changePassword = (event) => {
    setPassword(event.target.value);
  };
  const login = async () => {
    let errors = 0;
    const isValidNumDoc = numDocument.length > 0;
    setValidNumDocumento(isValidNumDoc);
    if (!isValidNumDoc) {
      errors = errors + 1;
    }

    const isValidPassword = password.length > 0;
    setValidPassword(isValidPassword);
    if (!isValidPassword) {
      errors = errors + 1;
    }

    if (errors === 0) {
      setLoading(true)
      const success = await loginAuth(tDoi, numDocument, password);
      setLoading(false);
      if (success) {
        window.location.href = '/accounts';
      }
    }
  };
  return (
    <LoginStyled>
      <MaskLoading show={loading} ></MaskLoading>
      <main className='login-container'>
        <div className='login-container__form'>
          <h2>{titleApp}</h2>
          <div className='form-body'>
            <FormControl className='form-white' variant='filled' fullWidth>
              <InputLabel id='demo-simple-select-label'>
                Tipo de documento
              </InputLabel>
              <Select
                labelId='demo-simple-select-label'
                id='demo-simple-select'
                value={tDoi}
                label='Tipo de documento'
                onChange={changeTDoi}
              >
                <MenuItem value={'L'}>DNI</MenuItem>
                <MenuItem value={'R'}>RUC</MenuItem>
                <MenuItem value={'C'}>CE</MenuItem>
              </Select>
            </FormControl>

            <FormControl className='form-white' variant='filled' fullWidth>
              <TextField
                error={!validNumDocumento}
                onChange={changeNumDocument}
                value={numDocument}
                label='Número de documento'
                variant='filled'
              />
            </FormControl>

            <FormControl className='form-white' variant='filled' fullWidth>
              <TextField
                error={!validPassword}
                id='pass'
                onChange={changePassword}
                value={password}
                label='Contraseña'
                type='password'
                variant='filled'
              />
            </FormControl>

            <FormControl className='form-button' variant='filled' fullWidth>
              <Button variant='contained' size='large' onClick={login}>
                Acceder
              </Button>
            </FormControl>
          </div>
        </div>
      </main>
    </LoginStyled>
  );
}

export default Login;
