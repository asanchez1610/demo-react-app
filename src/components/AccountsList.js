import React from 'react';
import styled from 'styled-components';
import Header from './Header';
import Container from '@mui/material/Container';
import Box from '@mui/material/Box';
import { formatAccount, formatAmout } from '../utils/Util';

const AccountsListStyled = styled.div`
  margin-top: 50px;
  table {
    border: 1px solid #ccc;
    border-collapse: collapse;
    margin: 0;
    padding: 0;
    width: 100%;
    table-layout: fixed;
  }

  table caption {
    font-size: 1.4em;
    margin: 0.7em 0 0.4em 0.75em;
    color: #1264a5;
  }

  table tr {
    background-color: #fff;
    border: 1px solid #ddd;
    padding: 0.35em;
    cursor: pointer;
  }

  table tr:hover {
    background-color: #b3e5fc;
  }

  table th,
  table td {
    padding: 0.625em;
    text-align: left;
    padding: 12px 16px;
  }

  table td {
    font-weight: lighter;
  }

  table th {
    font-size: 0.85em;
    letter-spacing: 0.1em;
    text-transform: uppercase;
    background-color: #004481;
    color: #fff;
  }

  .detail-account {
    line-height: 1.3em;
    b {
      color: #1264a5;
    }
  }

  @media screen and (max-width: 600px) {
    table {
      border: 0;
    }

    table caption {
      font-size: 1.3em;
    }

    table thead {
      border: none;
      clip: rect(0 0 0 0);
      height: 1px;
      margin: -1px;
      overflow: hidden;
      padding: 0;
      position: absolute;
      width: 1px;
    }

    table tr {
      border-bottom: 3px solid #ddd;
      display: block;
      margin-bottom: 0.625em;
      cursor: pointer;
    }

    table tr:hover {
      background-color: #b3e5fc;
    }

    table td {
      border-bottom: 1px solid #ddd;
      display: block;
      font-size: 0.8em;
      text-align: right;
    }

    table td::before {
      content: attr(data-label);
      float: left;
      font-weight: bold;
      text-transform: uppercase;
    }

    table td:last-child {
      border-bottom: 0;
    }
  }
`;

function AccountsList() {
  let customer = window.sessionStorage.getItem('customer');
  let financialOverview = window.sessionStorage.getItem('financialOverview');
  if (!customer) {
    window.location.href = '/';
  } else {
    customer = JSON.parse(customer);
  }
  if (financialOverview) {
    financialOverview = JSON.parse(financialOverview);
  } else {
    financialOverview = {};
  }
  const contracts = financialOverview.contracts;

  const goToDetail = (item) => {
    window.location.href = `/accounts/${item.id}`;
  };

  return (
    <AccountsListStyled>
      
      <Header titleHeader={'BIENVENIDO, ' + customer.firstName}></Header>
      <Container maxWidth='lg'>
        <Box
          sx={{
            bgcolor: '#f4f4f4',
            borderLeft: '1px solid #e1e1e1',
            borderRight: '1px solid #e1e1e1',
            minHeight: '100vh',
            padding: '0 15px',
            marginBottom: '25px',
          }}
        >
          <table>
            <caption>Mis Cuentas</caption>
            <thead>
              <tr>
                <th scope='col'>Tipo y Número de cuenta</th>
                <th scope='col'>Saldo contable</th>
                <th scope='col'>Saldo disponible</th>
              </tr>
            </thead>
            <tbody>
              {(contracts || []).map((item) => {
                return (
                  <tr onClick={() => goToDetail(item)}>
                    <td data-label='Tipo y Núm. cuenta'>
                      <span className='detail-account'>
                        <b>{formatAccount(item.number)}</b>
                        <br />
                        <small>{item.product.name}</small>
                      </span>
                    </td>
                    <td data-label='Saldo contable'>
                      {formatAmout(item.detail.specificAmounts, 'AVAILABLE_POSTED_BALANCES')}
                    </td>
                    <td data-label='Saldo disponible'>
                      {formatAmout(item.detail.specificAmounts, 'AVAILABLE_CURRENT_BALANCES')}
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </Box>
      </Container>
    </AccountsListStyled>
  );
}

export default AccountsList;
