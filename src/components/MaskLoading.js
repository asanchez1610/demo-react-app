import React from 'react';
import styled from 'styled-components';

const MaskLoadingStyled = styled.div`
  .container-loading {
    margin: 0;
    top: 0;
    left: 0;
    position: fixed;
    height: 100vh;
    width: 100vw;
    background-color: rgba(255, 255, 255, 0.7);
    display: flex;
    justify-content: center;
    align-items: center;
    z-index: 1000;
  }

  @keyframes ldio-ko41c0os71 {
    0% {
      transform: rotate(0deg);
    }
    50% {
      transform: rotate(180deg);
    }
    100% {
      transform: rotate(360deg);
    }
  }
  .ldio-ko41c0os71 div {
    position: absolute;
    animation: ldio-ko41c0os71 1s linear infinite;
    width: 160px;
    height: 160px;
    top: 20px;
    left: 20px;
    border-radius: 50%;
    box-shadow: 0 4px 0 0 #1d3f72;
    transform-origin: 80px 82px;
  }

  .loadingio-spinner-eclipse-2kbhlnvm6k3 {
    width: 200px;
    height: 200px;
    display: inline-block;
    overflow: hidden;
    background: rgba(255, 255, 255, 0);
  }

  .ldio-ko41c0os71 {
    width: 100%;
    height: 100%;
    position: relative;
    transform: translateZ(0) scale(1);
    backface-visibility: hidden;
    transform-origin: 0 0;
    /* see note above */
  }

  .ldio-ko41c0os71 div {
    box-sizing: content-box;
  }
`;

function MaskLoading({show}) {
  return (
    <MaskLoadingStyled>
      { show ?  <div class='container-loading'>
        <div class='loadingio-spinner-eclipse-2kbhlnvm6k3'>
          <div class='ldio-ko41c0os71'>
            <div></div>
          </div>
        </div>
      </div> : <span></span> }  
     
    </MaskLoadingStyled>
  );
}

export default MaskLoading;
