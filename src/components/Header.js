import React from 'react';
import styled from 'styled-components';
import PowerSettingsNew from '@mui/icons-material/PowerSettingsNew';
import KeyboardBackspace from '@mui/icons-material/KeyboardBackspace';

const HeaderStyled = styled.div`
    background-color: #072146;
    color: white;
    display: flex;
    align-items: center;
    height: 50px;
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    .actions {
        width: 45px;
        display: flex;
        align-items: center;
        justify-content: center;
        color: white;
        .logout {
            font-size: 1.8em;
            cursor: pointer;
        }
        .back {
            font-size: 1.8em;
            cursor: pointer;
        }
    }
    .title {
        width: calc(100% - 90px);
        display: flex;
        align-items: center;
        justify-content: center;
        color: white;
        font-size: 0.9em;
    }
`;

const logOut = () => {
    window.sessionStorage.removeItem('customer');
    window.sessionStorage.removeItem('tsec');
    window.location.href = '/';
};

function Header({titleHeader, showActionBack}) {
    return (
        <HeaderStyled>
            { !showActionBack ? <div className="actions">&nbsp;</div> : <div className="actions" onClick={ () => window.history.back() } ><KeyboardBackspace className="back" /></div> }
            <div className="title" >{titleHeader}</div>
            <div className="actions" onClick={ logOut } ><PowerSettingsNew className="logout" /></div>
        </HeaderStyled>
    );
}

export default Header;